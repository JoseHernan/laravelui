<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Client;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/comisiones', function () {

    $client = new Client([
    	'base_uri' => 'localhost:8000',
    ]);

    $response = $client->request('GET', 'api/comisiones');
    $comisiones = json_decode( $response->getBody()->getContents() );
    return view('comisiones.index', compact('comisiones'));
});

Route::get('/editar_notas/{id}', function ($id) {

    $client = new Client([
    	'base_uri' => 'localhost:8000',
    ]);

	$responseComision = $client->request('GET', 'api/comisiones/'.$id);
    $comision = json_decode( $responseComision->getBody()->getContents() );
    $comision = $comision[0];

    $responseAlumnos = $client->request('GET', 'api/alumnos/'.$id);
    $alumnos = json_decode( $responseAlumnos->getBody()->getContents() );

    return view('comisiones.editar', compact('comision','alumnos'));

});


Route::post('notas', 'NotaController@store');