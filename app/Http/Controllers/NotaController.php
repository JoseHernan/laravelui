<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class NotaController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$strJSON = '{"id": '. $request->input('comision'). ', "alumnos": [ ';
		foreach ($request->input('alumnos') as $key => $nota) {
			$strJSON .= '{"id": "' . $key .'", "notaFinal": "' . $nota . '"},';
		}
		$strJSON = trim($strJSON, ',');
		$strJSON .= ']}';

		$datos = json_decode($strJSON, true);

	  	$client = new Client([
	    	'base_uri' => 'localhost:8000',
	    ]);

		$response = $client->request('POST', 'api/alumnos/', ['json' => $datos ] );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function show($comision_id)
    {
    	//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AlumnoComision $alumnoComision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlumnoComision $alumnoComision)
    {
        //
    }

}
