<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Comisiones</title>
</head>
<body>
	<div class="container">
		<h1>Editar nota Final</h1>

		<table class="table">
		  <thead>
		    <tr>
		        <th scope="col">Materia</th>
		        <th scope="col">Catedra</th>
		        <th scope="col">Carrera</th>
				<th scope="col">Turno</th>
				<th scope="col">Semestre</th>
				<th scope="col">Año</th>
		    </tr>
		  </thead>
			<tr>
				<td>{{ $comision->nombre_materia }}</td>
				<td>{{ $comision->catedra }}</td>
				<td>{{ $comision->nombre_carrera }}</td>
				<td>{{ $comision->turno }}</td>
				<td>{{ $comision->semestre }}</td>
				<td>{{ $comision->anio }}</td>
			</tr>
		</table>
		
		<form action="/notas" method="post" enctype="application/x-www-form-urlencoded">
			{{ csrf_field() }}
			<input type="hidden" id="comision" name="comision" value="{{ $comision->id }}">
			@foreach($alumnos as $alumno)
			<div class="form-group">
			    <label for="{{ $alumno -> id }}">{{ $alumno -> apellido }}, {{ $alumno -> nombre }} </label>
			    <input type="text" name="alumnos[{{ $alumno -> id }}]" id="{{ $alumno -> id }}" value="{{ $alumno -> nota_final }}">
			    <hr />
			</div>			
			@endforeach
			
			<input type="submit" value="actualizar">
		</form>

	</div>
</body>
</html>