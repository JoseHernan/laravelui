<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Comisiones</title>
</head>
<body>
	<div class="container">
		<h1>Comisiones</h1>

		<table class="table table-dark">
		  <thead>
		    <tr>
		        <th scope="col">Materia</th>
		        <th scope="col">Catedra</th>
		        <th scope="col">Carrera</th>
				<th scope="col">Turno</th>
				<th scope="col">Semestre</th>
				<th scope="col">Año</th>
		        <th scope="col">Editar Notas</th>
		    </tr>
		  </thead>


		@foreach($comisiones as $comision)
		<tr>

			<td>{{ $comision->nombre_materia }}</td>
			<td>{{ $comision->catedra }}</td>
			<td>{{ $comision->nombre_carrera }}</td>
			<td>{{ $comision->turno }}</td>
			<td>{{ $comision->semestre }}</td>
			<td>{{ $comision->anio }}</td>

				@if ($comision->cantidad_alumnos > 0)
				<td>
					<a href="editar_notas/{{ $comision->id }}">
						<button type="button" class="btn btn-primary">Editar
							<span class="badge badge-light">{{ $comision->cantidad_alumnos }}</span>
						</button>
					</a>
				</td>
				@else 
				<td>No hay alumnos</td>
				@endif

		</tr>
		@endforeach
	</div>

</body>
</html>